load("nashorn:mozilla_compat.js");
java.io.File;

function desaturation(img) {
	
	var height = img.getHeight();
	var width = img.getWidth();

	var arr = new Array(width);
	for (i = 0; i < arr.length; i++)
		arr[i] = new Array(height);

	for (var i = 0; i < width; i++) {
		for (var j = 0; j < height; j++) {

			var rgb = img.getRGB(i, j);
			var r = (rgb >> 16) & 0xFF;
			var g = (rgb >> 8) & 0xFF;
			var b = (rgb & 0xFF);

			var grayLevel = (r + g + b) / 3;
			var gray = (grayLevel << 16) + (grayLevel << 8) + grayLevel;

			arr[i][j] = gray;
		}
	}

	return Java.to(arr, "int[][]");
}
