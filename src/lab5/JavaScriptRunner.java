package lab5;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import static java.lang.String.join;
import static java.lang.System.getProperty;
import static java.nio.file.Files.readAllLines;
import static java.nio.file.Paths.get;
import static javax.swing.JFileChooser.APPROVE_OPTION;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class JavaScriptRunner {
	private JTable table;
	private Map<String, List<String>> files = new HashMap<>();

	public static void main(String[] args) throws ScriptException, NoSuchMethodException, IOException {

		new JavaScriptRunner();
	}

	public JavaScriptRunner() {
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(null);
		frame.setSize(new Dimension(596, 345));

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(getProperty("user.home")));

		JButton btnLoadFile = new JButton("Load files");
		btnLoadFile.setBounds(12, 13, 141, 25);
		btnLoadFile.addActionListener(e -> fileChooser(btnLoadFile, fileChooser));
		frame.getContentPane().add(btnLoadFile);

		JButton btnRun = new JButton("Run");
		btnRun.setBounds(440, 13, 141, 25);
		frame.getContentPane().add(btnRun);
		btnRun.addActionListener(e -> runFunctions(btnRun, fileChooser));

		table = new JTable(new DefaultTableModel(new Object[] { "File", "Available methods" }, 0));
		table.setBounds(12, 58, 570, 211);
		JScrollPane tableContainer = new JScrollPane(table);
		tableContainer.setLocation(12, 50);
		tableContainer.setSize(569, 258);
		frame.getContentPane().add(tableContainer);

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		frame.setVisible(true);
	}

	private void runFunctions(JButton btnRun, JFileChooser fileChooser) {

		int selected = fileChooser.showOpenDialog(btnRun);

		if (selected == APPROVE_OPTION) {
			String selectedScript = (String) table.getValueAt(table.getSelectedRow(), 0);

			String fullPath = fileChooser.getSelectedFile().getAbsolutePath();

			files.get(selectedScript)
					.forEach(function -> executeFunction(selectedScript, fullPath, function));
		}

	}

	private void executeFunction(String selectedScript, String fullPath, String function) {
		
		try {
			ScriptEngineManager factory = new ScriptEngineManager();
			ScriptEngine engine = factory.getEngineByName("nashorn");

			BufferedImage img = ImageIO.read(new File(fullPath));
			engine.eval(new FileReader(selectedScript));

			int[][] result = null;

			result = (int[][]) ((Invocable) engine).invokeFunction(function, img);

			BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), TYPE_INT_RGB);

			for (int i = 0; i < img.getWidth(); i++) {
				for (int j = 0; j < img.getHeight(); j++) {
					image.setRGB(i, j, result[i][j]);
				}
			}

			File outputFile = new File(function + "_" + Paths.get(fullPath).getFileName().toString());

			ImageIO.write(image, "jpg", outputFile);

		} catch (NoSuchMethodException | ScriptException | IOException e) {
			e.printStackTrace();
		}
	}

	private void fileChooser(JButton btnLoadClass, JFileChooser fileChooser) {

		int result = fileChooser.showOpenDialog(btnLoadClass);

		if (result == APPROVE_OPTION) {
			String fullPath = fileChooser.getSelectedFile().getAbsolutePath();

			try {
				List<String> lines = readAllLines(get(fullPath));
				List<String> methods = new ArrayList<>();
				Pattern pattern = Pattern.compile("function (.*)\\(");

				lines.forEach(line -> {
					Matcher matcher = pattern.matcher(line);
					while (matcher.find()) {
						methods.add(matcher.group(1));
					}
				});
				String fileName = get(fullPath).getFileName().toString();
				files.put(fileName, methods);

				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new String[] { fileName, join(", ", methods) });

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
