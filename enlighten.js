load("nashorn:mozilla_compat.js");
java.io.File;

var enlightenColor = function(color, param){
	color += param;
	if (color > 255){
		color = 255;
	}else if(color < 0){
		color = 0;
	}
	
	
	return color;
}

function enlighten(img) {
	
	var param = javax.swing.JOptionPane.showInputDialog("Podaj stopien rozjasnienia")/1;
	
	var height = img.getHeight();
	var width = img.getWidth();

	var arr = new Array(width);
	for (i = 0; i < arr.length; i++)
		arr[i] = new Array(height);

	for (var i = 0; i < width; i++) {
		for (var j = 0; j < height; j++) {

			var rgb = img.getRGB(i, j);
			var r = enlightenColor((rgb >> 16) & 0xFF, param);
			var g = enlightenColor((rgb >> 8) & 0xFF, param);
			var b = enlightenColor((rgb & 0xFF), param);

			var result = (r << 16) + (g << 8) + b;

			arr[i][j] = result;
		}
	}

	return Java.to(arr, "int[][]");
}
